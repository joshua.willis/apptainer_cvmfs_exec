FROM igwn/base:conda

#USER root

RUN apt-get update --yes \
    && DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install --yes libxml2
RUN conda create -c conda-forge -n my-env python=3.12 cudatoolkit-dev
